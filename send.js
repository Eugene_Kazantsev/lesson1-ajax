let FormS = document.querySelector(".form-data");
//Слушатель событий
FormS.addEventListener('submit', function(e){

	e.preventDefault();

	let elem = e.target

	let formData = {
		name: elem.querySelector('[name="user_name"]').value,
		email: elem.querySelector('[name="user_email"]').value,
		phone: elem.querySelector('[name="user_phone"]').value,
		spam: elem.querySelector('[name="spam"]').value,
	};
	//POST-запрос
	axios.post('mail.php', {
		'user_name': formData.name,
		'user_email': formData.email,
		'user_phone': formData.phone,
		'spam': formData.spam,
	}).then(function(response){
		console.log('Заявка отправлена');
	}).catch(function(error){
		console.log('Ошибка. Данные не отправлены.');
	})
})